#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "conexion.h"
#include <QCheckBox>
#include <QTimer>


int salidas_b1;
int salidas_b2;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    int status=comunicacion();
    if(status < 0){
        Lost_Connection();
    }

    connect(timercin, SIGNAL(timeout()), this, SLOT(Main_Loop()));
    timercin->start(500);
    connect(timercout, SIGNAL(timeout()), this, SLOT(Reconnection_Loop()));




}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::Lost_Connection()
{
    ui->out_1->setEnabled(false);
    ui->out_2->setEnabled(false);
    ui->out_3->setEnabled(false);
    ui->out_4->setEnabled(false);
    ui->out_5->setEnabled(false);
    ui->out_6->setEnabled(false);
    ui->out_7->setEnabled(false);
    ui->out_8->setEnabled(false);
    ui->out_9->setEnabled(false);
    ui->out_10->setEnabled(false);
    ui->out_11->setEnabled(false);
    ui->out_12->setEnabled(false);
    ui->out_13->setEnabled(false);
    ui->out_14->setEnabled(false);
    ui->out_15->setEnabled(false);
    ui->out_16->setEnabled(false);
    ui->lbl_error->setText("Error de comunicacion");
    timercout->start(500);
}

void MainWindow::Inputs_Check()
{
    auto estado = leer();
    int estado_b1 = std::get<0>(estado);
    int estado_b2 = std::get<1>(estado);

    std::string word_b1= std::bitset<8>(estado_b1).to_string();
    QString str = QString::fromUtf8(word_b1.c_str());

    std::string word_b2= std::bitset<8>(estado_b2).to_string();
    QString str_b2 = QString::fromUtf8(word_b2.c_str());

        if(str_b2[7]=="1")
            ui->in_9->setChecked(true);
        else
            ui->in_9->setChecked(false);

        if(str_b2[6]=="1")
            ui->in_10->setChecked(true);
        else
            ui->in_10->setChecked(false);

        if(str_b2[5]=="1")
        ui->in_11->setChecked(true);
        else
            ui->in_11->setChecked(false);

        if(str_b2[4]=="1")
        ui->in_12->setChecked(true);
        else
            ui->in_12->setChecked(false);

        if(str_b2[3]=="1")
        ui->in_13->setChecked(true);
        else
            ui->in_13->setChecked(false);

        if(str_b2[2]=="1")
        ui->in_14->setChecked(true);
        else
            ui->in_14->setChecked(false);

        if(str_b2[1]=="1")
        ui->in_15->setChecked(true);
        else
            ui->in_15->setChecked(false);

        if(str_b2[0]=="1")
        ui->in_16->setChecked(true);
        else
            ui->in_16->setChecked(false);

        if(str[7]=="1")
        ui->in_1->setChecked(true);
        else
            ui->in_1->setChecked(false);

        if(str[6]=="1")
        ui->in_2->setChecked(true);
        else
            ui->in_2->setChecked(false);

        if(str[5]=="1")
        ui->in_3->setChecked(true);
        else
            ui->in_3->setChecked(false);

        if(str[4]=="1")
        ui->in_4->setChecked(true);
        else
            ui->in_4->setChecked(false);

        if(str[3]=="1")
        ui->in_5->setChecked(true);
        else
            ui->in_5->setChecked(false);

        if(str[2]=="1")
        ui->in_6->setChecked(true);
        else
            ui->in_6->setChecked(false);

        if(str[1]=="1")
        ui->in_7->setChecked(true);
        else
            ui->in_7->setChecked(false);

        if(str[0]=="1")
        ui->in_8->setChecked(true);
        else
            ui->in_8->setChecked(false);
        timercin->start();
}

void MainWindow::Restored_Connection(){
    ui->out_1->setEnabled(true);
    ui->out_2->setEnabled(true);
    ui->out_3->setEnabled(true);
    ui->out_4->setEnabled(true);
    ui->out_5->setEnabled(true);
    ui->out_6->setEnabled(true);
    ui->out_7->setEnabled(true);
    ui->out_8->setEnabled(true);
    ui->out_9->setEnabled(true);
    ui->out_10->setEnabled(true);
    ui->out_11->setEnabled(true);
    ui->out_12->setEnabled(true);
    ui->out_13->setEnabled(true);
    ui->out_14->setEnabled(true);
    ui->out_15->setEnabled(true);
    ui->out_16->setEnabled(true);
    ui->lbl_error->setText("");
    timercin->start(500);

}

void MainWindow::Main_Loop(){
    timercin->stop();
    int status=comunicacion();
    if(status>=0){
        Inputs_Check();
    }
    else{
        Lost_Connection();
    }
}

void MainWindow::Reconnection_Loop(){
    timercout->stop();
    int status=comunicacion();
    if (status>=0){
        Inputs_Check();
    }
    else{
        Lost_Connection();
    }
}

void MainWindow::on_out_1_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b1+=1;
    else
        salidas_b1-=1;
    escribir(salidas_b1, salidas_b2);
}

void MainWindow::on_out_2_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b1+=2;
    else
        salidas_b1-=2;
    escribir(salidas_b1, salidas_b2);
}

void MainWindow::on_out_3_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b1+=4;
    else
        salidas_b1-=4;
    escribir(salidas_b1, salidas_b2);
}

void MainWindow::on_out_4_stateChanged(int arg1)
{
    //2:activo 0:inactivo
    if(arg1==2)
        salidas_b1+=8;
    else
        salidas_b1-=8;
    escribir(salidas_b1, salidas_b2);
}

void MainWindow::on_out_5_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b1+=16;
    else
        salidas_b1-=16;
    escribir(salidas_b1,salidas_b2);
}

void MainWindow::on_out_6_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b1+=32;
    else
        salidas_b1-=32;
    escribir(salidas_b1,salidas_b2);
}

void MainWindow::on_out_7_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b1+=64;
    else
        salidas_b1-=64;
    escribir(salidas_b1, salidas_b2);
}

void MainWindow::on_out_8_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b1+=128;
    else
        salidas_b1-=128;
    escribir(salidas_b1, salidas_b2);
}

void MainWindow::on_out_9_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b2+=1;
    else
        salidas_b2-=1;
    escribir(salidas_b1, salidas_b2);

}

void MainWindow::on_out_10_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b2+=4;
    else
        salidas_b2-=4;
    escribir(salidas_b1, salidas_b2);

}

void MainWindow::on_out_11_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b2+=8;
    else
        salidas_b2-=8;
    escribir(salidas_b1, salidas_b2);

}

void MainWindow::on_out_12_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b2+=16;
    else
        salidas_b2-=16;
    escribir(salidas_b1, salidas_b2);

}

void MainWindow::on_out_13_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b2+=32;
    else
        salidas_b2-=32;
    escribir(salidas_b1, salidas_b2);

}

void MainWindow::on_out_14_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b2+=1;
    else
        salidas_b2-=1;
    escribir(salidas_b1, salidas_b2);

}

void MainWindow::on_out_15_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b2+=64;
    else
        salidas_b2-=64;
    escribir(salidas_b1, salidas_b2);

}

void MainWindow::on_out_16_stateChanged(int arg1)
{
    if(arg1==2)
        salidas_b2+=128;
    else
        salidas_b2-=128;
    escribir(salidas_b1, salidas_b2);

}

