#include "conexion.h"
using namespace std;

int comunicacion(){
    //**Variables de SeaMax**
    CSeaMaxLin Modulo;
    const char *portString = "sealevel_tcp://192.168.1.2";

    try
    {
        //OPEN
        int result = Modulo.Open(const_cast<char *>(portString));
        if (result != 0)
        {
            printf("[Error Modulo]: Error abrir Modulo, regreso de con '%d\n'", result);
           return -1;
        }
        if (result == 0)
        {
            printf("Conexion lograda\n");
        }

    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    Modulo.Close();
    sleep(1);
    return 0;
}
