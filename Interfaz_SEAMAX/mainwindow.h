#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

    QTimer *timercin =new QTimer(this);

    QTimer *timercout =new QTimer(this);

private slots:

    void on_out_1_stateChanged(int arg1);

    void on_out_2_stateChanged(int arg1);

    void on_out_3_stateChanged(int arg1);

    void on_out_4_stateChanged(int arg1);

    void on_out_5_stateChanged(int arg1);

    void on_out_6_stateChanged(int arg1);

    void on_out_7_stateChanged(int arg1);

    void on_out_8_stateChanged(int arg1);

    void on_out_9_stateChanged(int arg1);

    void on_out_10_stateChanged(int arg1);

    void on_out_11_stateChanged(int arg1);

    void on_out_12_stateChanged(int arg1);

    void on_out_13_stateChanged(int arg1);

    void on_out_14_stateChanged(int arg1);

    void on_out_15_stateChanged(int arg1);

    void on_out_16_stateChanged(int arg1);

    void Lost_Connection();

    void Inputs_Check();

    void Restored_Connection();

    void Main_Loop();

    void Reconnection_Loop();

private:
    Ui::MainWindow *ui;

};
#endif // MAINWINDOW_H
