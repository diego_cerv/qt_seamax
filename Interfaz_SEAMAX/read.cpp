#include "conexion.h"

using namespace std;

std::tuple<int ,int>leer(){
    //**Varibles de paso
    int b1=0;
    int b2=0;
    //int status=-1;
    //**Variables de SeaMax**
    CSeaMaxLin Modulo;
    const char *portString = "sealevel_tcp://192.168.1.2";

    slave_address_t	slaveId = 4;

    seaio_type_t	type;
    address_loc_t	start;
    address_range_t	range;

    unsigned char data[2] = {0, 0};

    try
    {
        //OPEN
        int result = Modulo.Open(const_cast<char *>(portString));
        if (result != 0)
        {
            printf("[Error Modulo]: Error abrir Modulo, regreso de read'%d\n'", result);
            return make_tuple(b1,b2);
            // return -1;
        }
        if (result == 0)
        {
            printf("Conexion lograda de read\n");

        }

        //INPUTS (read)
        type	= D_INPUTS;
        start	= 1;
        range	= 16;

        result = Modulo.Read(slaveId, type, start, range, data);
        if (result < 0)
        {
            printf("[Error Modulo]: Error leyendo entradas, regreso '%d\n'", result);
           return make_tuple(b1,b2);
            // return  -1;
        }
        else
        {
            printf("Inputs: %02X%02X\n", data[0], data[1]);
            b1=(int)data[0];
            b2=(int) data[1];
        }


    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    Modulo.Close();

    return make_tuple(b1,b2);
}
