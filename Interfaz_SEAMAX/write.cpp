#include "conexion.h"

using namespace std;

int escribir(int b1, int b2){
    //**Variables de SeaMax**
    CSeaMaxLin Modulo;
    const char *portString = "sealevel_tcp://192.168.1.2";

    slave_address_t	slaveId = 4;

    seaio_type_t	type;
    address_loc_t	start;
    address_range_t	range;

    unsigned char data[2] = {0, 0};

    try
    {
        //OPEN
        int result = Modulo.Open(const_cast<char *>(portString));
        if (result != 0)
        {
            printf("[Error Modulo]: Error abrir Modulo, regreso '%d\n'", result);
           return -1;
        }
        if (result == 0)
        {
            printf("Conexion lograda\n");
        }

        //*WRITE**
            type	= COILS;
            start	= 1;
            range	= 16;

            //memset(&data, num, sizeof(data));
            data[0]=b1;
            data[1]=b2;

            result = Modulo.Write(slaveId, type, start, range, data);

            if (result < 0)
            {
                printf("[Error Modulo]: Error escribiendo salidas, regreso '%d\n'", result);
                return -1;
            }
            else
            {
                printf("Write Outputs: %02X%02X\n", data[0], data[1]);
            }

    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    Modulo.Close();

    return 0;
}
