#ifndef CONEXION_H
#define CONEXION_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <seamaxlin.h>
#include <unistd.h>
#include <QString>
#include <bitset>
#include <tuple>
#include <iomanip>

std::tuple<int,int> leer();
int escribir(int b1,int b2);
int comunicacion();



#endif // CONEXION_H
