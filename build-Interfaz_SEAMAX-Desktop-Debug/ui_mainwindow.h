/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *layoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *out_1;
    QCheckBox *out_2;
    QCheckBox *out_3;
    QCheckBox *out_4;
    QCheckBox *out_5;
    QCheckBox *out_6;
    QCheckBox *out_7;
    QCheckBox *out_8;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *out_9;
    QCheckBox *out_10;
    QCheckBox *out_11;
    QCheckBox *out_12;
    QCheckBox *out_13;
    QCheckBox *out_14;
    QCheckBox *out_15;
    QCheckBox *out_16;
    QLabel *label;
    QLabel *label_2;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QCheckBox *in_1;
    QCheckBox *in_2;
    QCheckBox *in_3;
    QCheckBox *in_4;
    QCheckBox *in_5;
    QCheckBox *in_6;
    QCheckBox *in_7;
    QCheckBox *in_8;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *in_9;
    QCheckBox *in_10;
    QCheckBox *in_11;
    QCheckBox *in_12;
    QCheckBox *in_13;
    QCheckBox *in_14;
    QCheckBox *in_15;
    QCheckBox *in_16;
    QLabel *lbl_error;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(463, 436);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        layoutWidget_2 = new QWidget(centralwidget);
        layoutWidget_2->setObjectName(QStringLiteral("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(250, 50, 196, 271));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        out_1 = new QCheckBox(layoutWidget_2);
        out_1->setObjectName(QStringLiteral("out_1"));

        verticalLayout_3->addWidget(out_1);

        out_2 = new QCheckBox(layoutWidget_2);
        out_2->setObjectName(QStringLiteral("out_2"));

        verticalLayout_3->addWidget(out_2);

        out_3 = new QCheckBox(layoutWidget_2);
        out_3->setObjectName(QStringLiteral("out_3"));

        verticalLayout_3->addWidget(out_3);

        out_4 = new QCheckBox(layoutWidget_2);
        out_4->setObjectName(QStringLiteral("out_4"));

        verticalLayout_3->addWidget(out_4);

        out_5 = new QCheckBox(layoutWidget_2);
        out_5->setObjectName(QStringLiteral("out_5"));

        verticalLayout_3->addWidget(out_5);

        out_6 = new QCheckBox(layoutWidget_2);
        out_6->setObjectName(QStringLiteral("out_6"));

        verticalLayout_3->addWidget(out_6);

        out_7 = new QCheckBox(layoutWidget_2);
        out_7->setObjectName(QStringLiteral("out_7"));

        verticalLayout_3->addWidget(out_7);

        out_8 = new QCheckBox(layoutWidget_2);
        out_8->setObjectName(QStringLiteral("out_8"));

        verticalLayout_3->addWidget(out_8);


        horizontalLayout_2->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        out_9 = new QCheckBox(layoutWidget_2);
        out_9->setObjectName(QStringLiteral("out_9"));

        verticalLayout_4->addWidget(out_9);

        out_10 = new QCheckBox(layoutWidget_2);
        out_10->setObjectName(QStringLiteral("out_10"));

        verticalLayout_4->addWidget(out_10);

        out_11 = new QCheckBox(layoutWidget_2);
        out_11->setObjectName(QStringLiteral("out_11"));

        verticalLayout_4->addWidget(out_11);

        out_12 = new QCheckBox(layoutWidget_2);
        out_12->setObjectName(QStringLiteral("out_12"));

        verticalLayout_4->addWidget(out_12);

        out_13 = new QCheckBox(layoutWidget_2);
        out_13->setObjectName(QStringLiteral("out_13"));

        verticalLayout_4->addWidget(out_13);

        out_14 = new QCheckBox(layoutWidget_2);
        out_14->setObjectName(QStringLiteral("out_14"));

        verticalLayout_4->addWidget(out_14);

        out_15 = new QCheckBox(layoutWidget_2);
        out_15->setObjectName(QStringLiteral("out_15"));

        verticalLayout_4->addWidget(out_15);

        out_16 = new QCheckBox(layoutWidget_2);
        out_16->setObjectName(QStringLiteral("out_16"));

        verticalLayout_4->addWidget(out_16);


        horizontalLayout_2->addLayout(verticalLayout_4);

        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(60, 20, 91, 21));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(310, 20, 91, 21));
        label_2->setFont(font);
        layoutWidget = new QWidget(centralwidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 50, 196, 271));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        in_1 = new QCheckBox(layoutWidget);
        in_1->setObjectName(QStringLiteral("in_1"));
        in_1->setEnabled(false);

        verticalLayout->addWidget(in_1);

        in_2 = new QCheckBox(layoutWidget);
        in_2->setObjectName(QStringLiteral("in_2"));
        in_2->setEnabled(false);

        verticalLayout->addWidget(in_2);

        in_3 = new QCheckBox(layoutWidget);
        in_3->setObjectName(QStringLiteral("in_3"));
        in_3->setEnabled(false);

        verticalLayout->addWidget(in_3);

        in_4 = new QCheckBox(layoutWidget);
        in_4->setObjectName(QStringLiteral("in_4"));
        in_4->setEnabled(false);

        verticalLayout->addWidget(in_4);

        in_5 = new QCheckBox(layoutWidget);
        in_5->setObjectName(QStringLiteral("in_5"));
        in_5->setEnabled(false);

        verticalLayout->addWidget(in_5);

        in_6 = new QCheckBox(layoutWidget);
        in_6->setObjectName(QStringLiteral("in_6"));
        in_6->setEnabled(false);

        verticalLayout->addWidget(in_6);

        in_7 = new QCheckBox(layoutWidget);
        in_7->setObjectName(QStringLiteral("in_7"));
        in_7->setEnabled(false);

        verticalLayout->addWidget(in_7);

        in_8 = new QCheckBox(layoutWidget);
        in_8->setObjectName(QStringLiteral("in_8"));
        in_8->setEnabled(false);

        verticalLayout->addWidget(in_8);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        in_9 = new QCheckBox(layoutWidget);
        in_9->setObjectName(QStringLiteral("in_9"));
        in_9->setEnabled(false);

        verticalLayout_2->addWidget(in_9);

        in_10 = new QCheckBox(layoutWidget);
        in_10->setObjectName(QStringLiteral("in_10"));
        in_10->setEnabled(false);

        verticalLayout_2->addWidget(in_10);

        in_11 = new QCheckBox(layoutWidget);
        in_11->setObjectName(QStringLiteral("in_11"));
        in_11->setEnabled(false);

        verticalLayout_2->addWidget(in_11);

        in_12 = new QCheckBox(layoutWidget);
        in_12->setObjectName(QStringLiteral("in_12"));
        in_12->setEnabled(false);

        verticalLayout_2->addWidget(in_12);

        in_13 = new QCheckBox(layoutWidget);
        in_13->setObjectName(QStringLiteral("in_13"));
        in_13->setEnabled(false);

        verticalLayout_2->addWidget(in_13);

        in_14 = new QCheckBox(layoutWidget);
        in_14->setObjectName(QStringLiteral("in_14"));
        in_14->setEnabled(false);

        verticalLayout_2->addWidget(in_14);

        in_15 = new QCheckBox(layoutWidget);
        in_15->setObjectName(QStringLiteral("in_15"));
        in_15->setEnabled(false);

        verticalLayout_2->addWidget(in_15);

        in_16 = new QCheckBox(layoutWidget);
        in_16->setObjectName(QStringLiteral("in_16"));
        in_16->setEnabled(false);

        verticalLayout_2->addWidget(in_16);


        horizontalLayout->addLayout(verticalLayout_2);

        lbl_error = new QLabel(centralwidget);
        lbl_error->setObjectName(QStringLiteral("lbl_error"));
        lbl_error->setGeometry(QRect(0, 370, 441, 31));
        lbl_error->setFont(font);
        MainWindow->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        out_1->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
        out_2->setText(QApplication::translate("MainWindow", "2", Q_NULLPTR));
        out_3->setText(QApplication::translate("MainWindow", "3", Q_NULLPTR));
        out_4->setText(QApplication::translate("MainWindow", "4", Q_NULLPTR));
        out_5->setText(QApplication::translate("MainWindow", "5", Q_NULLPTR));
        out_6->setText(QApplication::translate("MainWindow", "6", Q_NULLPTR));
        out_7->setText(QApplication::translate("MainWindow", "7", Q_NULLPTR));
        out_8->setText(QApplication::translate("MainWindow", "8", Q_NULLPTR));
        out_9->setText(QApplication::translate("MainWindow", "9", Q_NULLPTR));
        out_10->setText(QApplication::translate("MainWindow", "10", Q_NULLPTR));
        out_11->setText(QApplication::translate("MainWindow", "11", Q_NULLPTR));
        out_12->setText(QApplication::translate("MainWindow", "12", Q_NULLPTR));
        out_13->setText(QApplication::translate("MainWindow", "13", Q_NULLPTR));
        out_14->setText(QApplication::translate("MainWindow", "14", Q_NULLPTR));
        out_15->setText(QApplication::translate("MainWindow", "15", Q_NULLPTR));
        out_16->setText(QApplication::translate("MainWindow", "16", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Entradas", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Salidas", Q_NULLPTR));
        in_1->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
        in_2->setText(QApplication::translate("MainWindow", "2", Q_NULLPTR));
        in_3->setText(QApplication::translate("MainWindow", "3", Q_NULLPTR));
        in_4->setText(QApplication::translate("MainWindow", "4", Q_NULLPTR));
        in_5->setText(QApplication::translate("MainWindow", "5", Q_NULLPTR));
        in_6->setText(QApplication::translate("MainWindow", "6", Q_NULLPTR));
        in_7->setText(QApplication::translate("MainWindow", "7", Q_NULLPTR));
        in_8->setText(QApplication::translate("MainWindow", "8", Q_NULLPTR));
        in_9->setText(QApplication::translate("MainWindow", "9", Q_NULLPTR));
        in_10->setText(QApplication::translate("MainWindow", "10", Q_NULLPTR));
        in_11->setText(QApplication::translate("MainWindow", "11", Q_NULLPTR));
        in_12->setText(QApplication::translate("MainWindow", "12", Q_NULLPTR));
        in_13->setText(QApplication::translate("MainWindow", "13", Q_NULLPTR));
        in_14->setText(QApplication::translate("MainWindow", "14", Q_NULLPTR));
        in_15->setText(QApplication::translate("MainWindow", "15", Q_NULLPTR));
        in_16->setText(QApplication::translate("MainWindow", "16", Q_NULLPTR));
        lbl_error->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
