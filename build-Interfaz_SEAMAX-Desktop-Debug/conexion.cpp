#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <seamaxlin.h>
#include <unistd.h>

using namespace std;
int main()
{
	//**Variables de SeaMax**
	CSeaMaxLin Modulo;

	const char *portString = "sealevel_tcp://192.168.1.2";
	
	slave_address_t	slaveId = 4;

	seaio_type_t	type;
	address_loc_t	start;
	address_range_t	range;

	unsigned char data[2] = {0, 0};

	try
    {
		//OPEN
		int result = Modulo.Open(const_cast<char *>(portString));
		if (result != 0)
		{
			printf("[Error Modulo]: Error abrir Modulo, regreso '%d\n'", result);
			return -1;
		}
		if (result == 0)	
		{
			printf("Conexion lograda\n");
		}

		//INPUTS (read)
		type	= D_INPUTS;
		start	= 1;
		range	= 16;

		result = Modulo.Read(slaveId, type, start, range, data);
		if (result < 0)
		{
			printf("[Error Modulo]: Error leyendo entradas, regreso '%d\n'", result);
			return -1;
		}
		else
		{
			printf("Inputs: %02X%02X\n", data[0], data[1]);
		}
			//OUTPUTS (read)
		type	= COILS;
		start	= 1;
		range	= 16;
        
        result = Modulo.Read(slaveId, type, start, range, data);

		if (result < 0)
		{
			printf("[Error Modulo]: Error leyendo salidas, regreso '%d\n'<", result);
			return -1;
		}
		else
		{
			printf("Outputs: %02X%02X\n", data[0], data[1]);
		}

		Modulo.Close();
		sleep(1);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
		
	
/**WRITE**
	type	= COILS;
	start	= 1;
	range	= 16;

	memset(&data, 0x00, sizeof(data));

	result = Modulo.Write(slaveId, type, start, range, data);

	if (result < 0)
	{
		printf("[Error Modulo]: Error escribiendo salidas, regreso '%d\n'", result);
		return -1;
	}
	else
	{
		printf("Write Outputs: %02X%02X\n", data[0], data[1]);
	}
	*/

	return 0;
}