/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Interfaz_SEAMAX/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[24];
    char stringdata0[453];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 21), // "on_out_1_stateChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 4), // "arg1"
QT_MOC_LITERAL(4, 39, 21), // "on_out_2_stateChanged"
QT_MOC_LITERAL(5, 61, 21), // "on_out_3_stateChanged"
QT_MOC_LITERAL(6, 83, 21), // "on_out_4_stateChanged"
QT_MOC_LITERAL(7, 105, 21), // "on_out_5_stateChanged"
QT_MOC_LITERAL(8, 127, 21), // "on_out_6_stateChanged"
QT_MOC_LITERAL(9, 149, 21), // "on_out_7_stateChanged"
QT_MOC_LITERAL(10, 171, 21), // "on_out_8_stateChanged"
QT_MOC_LITERAL(11, 193, 21), // "on_out_9_stateChanged"
QT_MOC_LITERAL(12, 215, 22), // "on_out_10_stateChanged"
QT_MOC_LITERAL(13, 238, 22), // "on_out_11_stateChanged"
QT_MOC_LITERAL(14, 261, 22), // "on_out_12_stateChanged"
QT_MOC_LITERAL(15, 284, 22), // "on_out_13_stateChanged"
QT_MOC_LITERAL(16, 307, 22), // "on_out_14_stateChanged"
QT_MOC_LITERAL(17, 330, 22), // "on_out_15_stateChanged"
QT_MOC_LITERAL(18, 353, 22), // "on_out_16_stateChanged"
QT_MOC_LITERAL(19, 376, 15), // "Lost_Connection"
QT_MOC_LITERAL(20, 392, 12), // "Inputs_Check"
QT_MOC_LITERAL(21, 405, 19), // "Restored_Connection"
QT_MOC_LITERAL(22, 425, 9), // "Main_Loop"
QT_MOC_LITERAL(23, 435, 17) // "Reconnection_Loop"

    },
    "MainWindow\0on_out_1_stateChanged\0\0"
    "arg1\0on_out_2_stateChanged\0"
    "on_out_3_stateChanged\0on_out_4_stateChanged\0"
    "on_out_5_stateChanged\0on_out_6_stateChanged\0"
    "on_out_7_stateChanged\0on_out_8_stateChanged\0"
    "on_out_9_stateChanged\0on_out_10_stateChanged\0"
    "on_out_11_stateChanged\0on_out_12_stateChanged\0"
    "on_out_13_stateChanged\0on_out_14_stateChanged\0"
    "on_out_15_stateChanged\0on_out_16_stateChanged\0"
    "Lost_Connection\0Inputs_Check\0"
    "Restored_Connection\0Main_Loop\0"
    "Reconnection_Loop"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  119,    2, 0x08 /* Private */,
       4,    1,  122,    2, 0x08 /* Private */,
       5,    1,  125,    2, 0x08 /* Private */,
       6,    1,  128,    2, 0x08 /* Private */,
       7,    1,  131,    2, 0x08 /* Private */,
       8,    1,  134,    2, 0x08 /* Private */,
       9,    1,  137,    2, 0x08 /* Private */,
      10,    1,  140,    2, 0x08 /* Private */,
      11,    1,  143,    2, 0x08 /* Private */,
      12,    1,  146,    2, 0x08 /* Private */,
      13,    1,  149,    2, 0x08 /* Private */,
      14,    1,  152,    2, 0x08 /* Private */,
      15,    1,  155,    2, 0x08 /* Private */,
      16,    1,  158,    2, 0x08 /* Private */,
      17,    1,  161,    2, 0x08 /* Private */,
      18,    1,  164,    2, 0x08 /* Private */,
      19,    0,  167,    2, 0x08 /* Private */,
      20,    0,  168,    2, 0x08 /* Private */,
      21,    0,  169,    2, 0x08 /* Private */,
      22,    0,  170,    2, 0x08 /* Private */,
      23,    0,  171,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_out_1_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_out_2_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_out_3_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_out_4_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_out_5_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->on_out_6_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_out_7_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_out_8_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_out_9_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_out_10_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_out_11_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_out_12_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_out_13_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_out_14_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->on_out_15_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_out_16_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->Lost_Connection(); break;
        case 17: _t->Inputs_Check(); break;
        case 18: _t->Restored_Connection(); break;
        case 19: _t->Main_Loop(); break;
        case 20: _t->Reconnection_Loop(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 21;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
